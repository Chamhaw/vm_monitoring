$(document).ready(function() {
    var h_check = $("#vm_list thead :checkbox");
    var b_check = $("#vm_list tbody :checkbox")
    h_check.click(function() {
        if (this.checked) {
            b_check.prop("checked", "checked");
        } else {
            b_check.removeProp("checked");
        }
    })
    b_check.click(function() {
        chk = $("#vm_list tbody :checkbox:checked").length
        // console.log(chk)
        if (chk == b_check.length) {
            h_check.prop("checked", "checked")
        } else {
            h_check.removeProp("checked")
        }
    });
    $("#vm_action button[name='delete_vm']").click(function() {
        vm_uuid_list = [];
        $("#vm_list tbody :checkbox:checked").each(function() {
            vm_uuid_list.push($(this).val());
        });
        if ((vm_uuid_list.length) == 0) {
            alert("No VM selected.");
            return;
        }
        console.log(vm_uuid_list)
        if (confirm("Are you sure to delete the VM(s) from disk?")) {
            $.ajax({
                type: "POST",
                url: "/vsphere/delete",
                data: { "vms_uuid_list": JSON.stringify(vm_uuid_list) },
                success: function(result) {
                    if (result.ret_code == 1000) {
                        console.log(result.message)
                        window.location.href = '.'
                    }
                },
                error: function() {
                    window.location.href = '.'
                }
            })
        }
    });

    $("#vm_action button[name='poweron_vm']").click(function() {
        vm_uuid_list = [];
        $("#vm_list tbody :checkbox:checked").each(function() {
            vm_uuid_list.push($(this).val());
        });

        if (vm_uuid_list.length <= 0) {
            alert("No VM selected.");
            return;
        }
        if (confirm("Are you sure to poweron the VM(s)?")) {
            $.ajax({
                type: "POST",
                url: "/vsphere/poweron",
                data: { vms_uuid_list: JSON.stringify(vm_uuid_list) },
                success: function(result) {
                    if (result.ret_code == 1000) {
                        console.log(result.message)
                        window.location.href = "."
                    }
                },
                error: function() {
                    window.location.href = "."
                }
            });
        }
    });



    $("#vm_action button[name='poweroff_vm']")
        .click(function() {
            vm_uuid_list = [];

            console.log("vm_detail hidden")
            $("#vm_list tbody :checkbox:checked").each(function() {
                vm_uuid_list.push($(this).val());
            });

            if (vm_uuid_list.length <= 0) {
                alert("No VM selected.");
                return;
            }

            if (confirm("Are you sure to poweroff the VM(s)?")) {
                $.ajax({
                    type: "POST",
                    url: "/vsphere/poweroff",
                    data: { vms_uuid_list: JSON.stringify(vm_uuid_list) },
                    success: function(result) {
                        if (result.ret_code == 1000) {
                            console.log(result.message)
                            window.location.href = "."
                        }
                    },
                    error: function() {
                        window.location.href = "."
                    }
                });
            }
        });

    $("[data-toggle='popover']").each(function() {
        var $pElem = $(this);
        $pElem.popover({
            placement: 'top',
            container: 'body',
            content: 'Not support yet',
            trigger: 'hover'
        })
    });

    h_removecheck = $("#remove_disk thead :checkbox");
    b_removecheck = $("#remove_disk tbody :checkbox")
    h_removecheck.click(function () {
        if (this.checked) {
            b_removecheck.prop("checked", "checked");
        } else {
            b_removecheck.removeProp("checked");
        }
    });
    b_removecheck.click(function () {
        chk = $("#remove_disk tbody :checkbox:checked").length
        // console.log(chk)
        if (chk == b_removecheck.length) {
            h_removecheck.prop("checked", "checked")
        } else {
            h_removecheck.removeProp("checked")
        }
    });
});


// function vm_detail(obj) {



//     data = obj.getAttribute("data")
//         // .replace(/[^'"](\d+)L(?!\s*(['"]|\w+))/g, "$1")
//         .replace(/'(?=(\w+|-*|\s*[,\}:]))/g, '"')
//     // console.log(data)
//     $("#vm_detail").attr("data", data)
//     data = JSON.parse(data);
//     uuid = data.uuid
//     $.ajax({
//         type: "GET",
//         url: "/vsphere/detail/" + uuid,
//         success: function(result) {
//             console.log(result)
//             if (result.ret_code == 1000) {
//                 console.log(result.message)
//             }
//         },
//         error: function() {
//         }
//     });
    // $("#vm_list").attr("style", "visibility: hidden; display:none");
    // $("#vm_detail").attr("style", "visibility: visible");
    // $("#vm_detail").attr("data", data)
    // $("#vm_detail div[name='vm_name']").text(data.name)
    // $("#vm_detail td[name='ip_addr']").text(data.ip)
    // $("#vm_detail td[name='disk_size']").text(data.total_capacity)
    // $("#vm_detail td[name='power_sta']").text(data.state)
    // $("#vm_detail td[name='nbva_ver']").text(data.version);
    // $("#vm_detail td[name='hostname']").text(data.hostname);
    // $("#vm_detail td[name='cpu_number']").text(data.numCPU);
    // $("#vm_detail td[name='mem_size']").text(data.memoryMB);
    // console.log($("#vm_detail").attr("data"))
// }

function add_disk() {
    $("#disk_info").modal();
}

function remove_disk() {
    $("#remove_disk").modal();
}

function fillNetworkSetting() {
    $("[name='dnsdomain']").val("cdc.veritas.com");
    $("[name='dnsserver']").val("172.16.8.32");
    $("[name='netmask']").val("255.255.252.0");
    $("[name='gateway']").val("10.200.68.1");
}

function AddDiskCheckboxChange() {
    var checkedFlag = document.getElementById("independenbox").checked;
    
    if (checkedFlag == true) {
        $('#independenradio1').removeAttr("disabled")
        $('#independenradio2').removeAttr("disabled")
        //$('#independenradio1').attr("disabled", "disabled")
        //$('#independenradio2').attr("disabled", "disabled")
    }
    else {
        //$('#independenradio1').removeAttr("disabled")
        //$('#independenradio2').removeAttr("disabled")
        $('#independenradio1').attr("disabled", "disabled")
        $('#independenradio2').attr("disabled", "disabled")
    }
}


function poweron_vm(uuid) {
    if (confirm("Are you sure to poweron the VM?")) {
        $.ajax({
            type: "POST",
            url: "/vsphere/poweron",
            data: { vms_uuid_list:  JSON.stringify(uuid) },
            success: function(result) {
                if (result.ret_code == 1000) {
                    window.location.href = "."
                }
            },
            error: function() {
                window.location.href = "."
            }
        });
    };
}

function poweroff_vm(uuid) {
    if (confirm("Are you sure to poweroff the VM?")) {
        $.ajax({
            type: "POST",
            url: "/vsphere/poweroff",
            data: { vms_uuid_list: JSON.stringify(uuid)},
            success: function(result) {
                if (result.ret_code == 1000) {
                    console.log(result.message)
                    window.location.href = "."
                }
            },
            error: function() {
                window.location.href = "."
            }
        });
    };
}