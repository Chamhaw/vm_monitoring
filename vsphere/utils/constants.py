"""
Constants
"""
VM_INFO_SAMPLE = {
    "total_disk_capacity": "110.50 TB",
    "total_thick_disk_capacity": "35.31 TB",
    "total_thin_disk_capacity": "75.19 TB",
    "total_vm_number": 42,
    "vms": [
        {
            "hostname": "fwheiofhweoih",
            "ip": "10.200.71.55",
            "memoryMB": 16384,
            "name": "Richard_NBVA748_71_86",
            "numCPU": 8,
            "state": "Off",
            "total_capacity": "2.60 TB",
            "total_capacity_in_kb": 2791309312,
            "total_thick_capacity_in_kb": 0L,
            "total_thin_capacity_in_kb": 2791309312L,
            "uuid": "4204a854-f49a-8c3e-ee11-d6e270fb4fcb",
            "version": "3.0"
        },
        {
            "hostname": "n71-h136.cdc.veritas.com",
            "ip": "-",
            "memoryMB": 16384,
            "name": "WangXi-h136-3.0",
            "numCPU": 8,
            "state": "Off",
            "total_capacity": "2.60 TB",
            "total_capacity_in_kb": 2791309312L,
            "total_thick_capacity_in_kb": 0L,
            "total_thin_capacity_in_kb": 2791309312L,
            "uuid": "42048699-62d8-c37a-b591-a4cc99a5ad3c",
            "version": "3.0"
        },
        {
            "hostname": "-",
            "ip": "-",
            "memoryMB": 16384,
            "name": "n71-h105",
            "numCPU": 8,
            "state": "Off",
            "total_capacity": "2.60 TB",
            "total_capacity_in_kb": 2791309312L,
            "total_thick_capacity_in_kb": 2791309312L,
            "total_thin_capacity_in_kb": 0L,
            "uuid": "42044a68-eca9-e207-d8d0-ac5fe3a64a83",
            "version": "3.0"
        }
    ]
}
