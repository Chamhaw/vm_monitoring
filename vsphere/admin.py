from django.contrib import admin
from .models import VMInfo, VMSumInfo, VMDiskInfo

# Register your models here.
admin.site.register(VMInfo)
admin.site.register(VMSumInfo)
admin.site.register(VMDiskInfo)
