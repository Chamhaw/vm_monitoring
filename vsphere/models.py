from __future__ import unicode_literals

from django.db import models

# Create your models here.
class DeployParam(models.Model):
    ovaFilePath = models.TextField()
    dataStore = models.TextField()
    hostName = models.TextField()
    ipAddress = models.TextField()
    dnsDomain = models.TextField()
    dnsServer = models.TextField()
    netmask = models.TextField()
    gateway = models.TextField()
    adPoolName = models.TextField()
    adStuName = models.TextField()
    msdpPoolName = models.TextField()
    msdpStuName = models.TextField()


class VMInfo(models.Model):
    uuid = models.CharField(max_length=60, default='', unique=True)
    name = models.CharField(max_length=60, default='-')
    ip = models.CharField(max_length=60, default='-')
    hostname = models.CharField(max_length=60, default='')
    total_capacity = models.CharField(max_length=20, default='')
    total_capacity_in_kb = models.IntegerField(default=0)
    version = models.CharField(max_length=20, default='')
    state = models.CharField(max_length=10, default='')
    numCPU = models.IntegerField(default=8)
    memoryMB = models.IntegerField(default=0)
    disk_size = models.CharField(max_length=60, default='')

    def to_dict(self):
        self._state = ""
        return self.__dict__


class VMSumInfo(models.Model):
    total_vm_number = models.IntegerField()
    total_disk_capacity = models.CharField(max_length=60)
    total_thin_disk_capacity = models.CharField(max_length=60)
    total_thick_disk_capacity = models.CharField(max_length=60)
    total_datastore_capacity = models.CharField(max_length=60)
    total_left_datastore_capacity = models.CharField(max_length=60)
    total_provision_datastore_capacity = models.CharField(max_length=60)


class VMDiskInfo(models.Model):
    diskMode = models.CharField(max_length=60)
    vm_uuid = models.ForeignKey(VMInfo, to_field="uuid")
    uuid = models.CharField(max_length=60)
    wwid = models.CharField(max_length=60)
    size = models.CharField(max_length=60)
    label = models.CharField(max_length=60)
    unitNumber = models.IntegerField(default=0)
    capacityInKB = models.BigIntegerField(default=0)
    thinProvisioned = models.BooleanField(default=True)

def get_vms_data(orderkey=None, filterkey=None, **kwargs):
    # if orderkey:
        # VMInfo.object.order_by(orderkey)
    vm_info = VMInfo.objects.values()
    return queryset_en(vm_info, orderkey=orderkey,
                       filterkey=filterkey, kwargs=None)

def get_vm_sum(orderkey=None, filterkey=None):
    vm_sum_info = VMSumInfo.objects.values()
    return queryset_en(vm_sum_info, orderkey=orderkey,
                       filterkey=filterkey)[0]

def queryset_en(queryset, orderkey=None, filterkey=None, **kwargs):
    if orderkey:
        queryset = queryset.order_by(orderkey)
    if kwargs.get('reverse'):
        print kwargs.get('reverse')
        queryset = queryset.reverse()
    if filterkey:
        queryset = queryset.filter(filterkey)
    if kwargs.get('exclude'):
        queryset = queryset.exclude(kwargs['exclude'])
    return queryset
